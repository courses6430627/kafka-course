package pablohinojosa.org.demos.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class ProducerDemo {

    private static final Logger log = LoggerFactory.getLogger(ProducerDemo.class.getSimpleName());

    public static void main(String[] args) {
        log.info("I am a kafka producer");

        // create producer properties
        Properties properties = new Properties();

        // Connect to localhost
//        properties.setProperty("bootstrap.servers", "127.0.0.1:9092");

        // Connect to Conduktor playground
        properties.setProperty("bootstrap.servers", "cluster.playground.cdkt.io:9092");
        properties.setProperty("security.protocol", "SASL_SSL");
        properties.setProperty("sasl.mechanism", "PLAIN");
        properties.setProperty("sasl.jaas.config", "org.apache.kafka.common.security.plain.PlainLoginModule required username='pablohinojosacompany' password='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2F1dGguY29uZHVrdG9yLmlvIiwic291cmNlQXBwbGljYXRpb24iOiJhZG1pbiIsInVzZXJNYWlsIjpudWxsLCJwYXlsb2FkIjp7InZhbGlkRm9yVXNlcm5hbWUiOiJwYWJsb2hpbm9qb3NhY29tcGFueSIsIm9yZ2FuaXphdGlvbklkIjo3NDI5NCwidXNlcklkIjpudWxsLCJmb3JFeHBpcmF0aW9uQ2hlY2siOiJiYTU0MGJlMS1hZmZlLTQyZTMtYjk4MS04ZTI5NGIwNjUzNWUifX0.kQDqj-v30ZTlvb7ZWueIp5LH_99CAsnM68TAlCpS8sA';");

        properties.setProperty("key.serializer", StringSerializer.class.getName());
        properties.setProperty("value.serializer", StringSerializer.class.getName());

        // create the producer
        KafkaProducer<String, String> producer = new KafkaProducer<>(properties);

        // create a producer Record
        ProducerRecord<String, String> producerRecord = new ProducerRecord<>("demo_java", "Hello world!!");

        // send data
        producer.send(producerRecord);

        // tell the producer to send all data block until done -- synchronous
        producer.flush();

        // flush and close the producer
        producer.close();


    }
}
